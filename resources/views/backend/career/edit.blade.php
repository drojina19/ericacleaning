@extends('backend.layout.app')
@section('main-section')

    <div class="box-header">
        <div class="box-header with-border">
            <section class="content-header">
                <h1>
                    <b>Edit Career</b>
                </h1>


            </section>

        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">


                        <div class="row">
                            <div class="col-md-12">
                                <form method="POST" class="form-" enctype="multipart/form-data" action="{{route('admin.career.update',$career->id)}}">

                                    {{csrf_field()}}
                                    {{method_field("PATCH")}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="hidden" name="_token" value="{{CSRF_token()}}">

                                            <div class="form-group">
                                                <label for="name" class="control-label">Designation *</label>
                                                <input type="text" name="title" id="title" class="form-control" value="{{$career->designation}}" required>
                                            </div>

                                            <div class="form-group">
                                                <label for="description" class="control-label">Description</label>
                                                <textarea name="description" id="description" class="form-control" rows="5" required>{{$career->description}}</textarea>
                                            </div>


                                            <div class="form-group">
                                                <label for="" class="control-label col-md-2">Image *</label>
                                                <input type="file" name="image" id="image" class="form-control " required>
                                                <small style="color: red">Please upload the image of size 585 x 454 px</small>
                                            </div>


                                            <div class="form-group">
                                                <label for="status" class="control-label">Status *</label>
                                                <select class="form-control"  aria-label="Example select with button addon" name="status">
                                                    <option value="1" {{$career->status==1?'selected':''}}>Active</option>
                                                    <option value="0" {{$career->status==0?'selected':''}}>Inactive</option>
                                                </select>
                                            </div>
                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-success">Update</button>
                                                <a href="{{route('admin.career.index')}}" class="btn btn-danger pull-right">Cancel</a>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
