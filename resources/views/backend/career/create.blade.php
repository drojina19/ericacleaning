@extends('backend.layout.app')
@section('main-section')

    <div class="box-header">
        <div class="box-header with-border">
            <section class="content-header">
                <h1>
                    <b>Create Career</b>
                </h1>


            </section>

        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">


                        <div class="row">
                            <div class="col-md-12">
                                <form method="POST" class="form-" enctype="multipart/form-data" action="{{route('admin.career.store')}}">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="hidden" name="_token" value="{{CSRF_token()}}">
                                            <div class="form-group">
                                                <label for="name" class="control-label">Designation *</label>
                                                <input type="text" name="designation" id="designation" class="form-control" required>
                                            </div>

                                            <div class="form-group">
                                                <label for="description" class="control-label">Description</label>
                                                <textarea name="description" id="description" class="form-control" rows="5" required></textarea>
                                            </div>


                                            <div class="form-group">
                                                <label for="" class="control-label col-md-2">Image *</label>
                                                <input type="file" name="image" id="image" class="form-control " required>
                                                <small style="color: red">Please upload the image of size 585 x 454 px</small>
                                            </div>

                                            <div class="form-group">
                                                <label for="status" class="control-label">Status *</label>
                                                <select name="status" class="form-control" required>
                                                    <option value="1">Active</option>
                                                    <option value="0">InActive</option>
                                                </select>
                                            </div>
                                            <div class="box-footer">
                                                <button type="submit" class="btn btn-success">Create</button>
                                                <a href="{{route('admin.career.index')}}" class="btn btn-danger pull-right">Cancel</a>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
