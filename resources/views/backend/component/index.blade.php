@extends('backend.layout.app')
@section('main-section')

    <div class="box-header">
        <div class="box-header with-border">
            <section class="content-header">
                <h1>
                    <b>List Component</b>
                </h1>
            </section>

        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <!-- /.box-header -->
                    @if(Session::has('flash'))
                        <div class="alert alert-success">
                            {{Session::get('flash')}}
                        </div>

                    @endif()
                    <div class="box-body">

                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr class="trHighlight">
                                <th>Sn</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $sn=1;?>
                            @if(count($components)>0)
                                @foreach($components as $component)
                                    <tr>
                                        <td>{{$sn}}</td>
                                        <td>{{$component->title}}</td>

                                        <td><img src="{{asset('public/images/component/'.$component->image)}}" alt="" class="img img-responsive" style="width:200px"> </td>

                                        <td>{!!$component->description!!}</td>


                                        <td>{{$component->status==1?"Acitve":"Inactive"}}</td>
                                        <td>
                                            <a href="{{url('admin/component/'.$component->id.'/edit')}}" ><i class="fa fa-edit" title="Edit" ></i></a>

                                        </td>
                                    </tr>
                                    <?php $sn++;?>
                                @endforeach()

                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->

@endsection