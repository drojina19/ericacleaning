@extends('backend.layout.app')
@section('main-section')

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <!-- /.box-header -->
                    @if(Session::has('flash'))
                        <div class="alert alert-success">
                            {{Session::get('flash')}}
                        </div>

                    @endif()
                    <div class="box-body">
                        <div class="col-md-6" style="padding-left: 0px">
                            <div class="col-md-6" style="padding-left: 0px;padding-bottom:5px">
                                <a href="{{route('admin.team.create')}}" class title="Create">
                                    <span class="badge badge-success">
                                    <i class="fa fa-plus"></i>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr class="trHighlight">
                                <th>Sn</th>
                                <th>Name</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $sn=1;?>
                            @if(count($teams)>0)
                                @foreach($teams as $tm)
                                    <tr>
                                        <td>{{$sn}}</td>
                                        <td>{{$tm->name}}</td>
                                        <td>{{$tm->title}}</td>
                                        <td><img src="{{asset('public/images/team/'.$tm->image)}}" alt="" class="img img-responsive" style="width:200px"> </td>
                                        <td>{!!$tm->description!!}</td>
                                        <td>{{$tm->status==1?"Active":"Inactive"}}</td>
                                        <td>
                                            <a href="{{url('admin/team/'.$tm->id.'/edit')}}" ><i class="fa fa-edit" title="Edit" ></i></a>
                                            |
                                            <form action="{{route('admin.team.destroy',['team'=>$tm->id])}}" method="POST" style="display: inline">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-danger btn-xs" onclick="return confirm(' Are You Sure to Delete?')"><i class="fa fa-trash" title="Delete" ></i></button>
                                            </form>


                                        </td>
                                    </tr>
                                    <?php $sn++;?>
                                @endforeach()

                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->

@endsection
