@extends('backend.layout.app')
@section('main-section')


    <div class="box-header">
        <div class="box-header with-border">
            <section class="content-header">
                <h1>
                    <b>List Slider</b>
                </h1>

                <div class="col-md-6" style="padding-left: 0px">
                    <div class="col-md-6" style="padding-left: 0px;padding-bottom:5px">
                        <a href="{{route('admin.slider.create')}}" class title="Create">
                                    <span class="badge badge-success">
                                    <i class="fa fa-plus"></i>
                                    </span>
                        </a>
                    </div>
                </div>
            </section>

        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <!-- /.box-header -->
                    @if(Session::has('flash'))
                        <div class="alert alert-success">
                            {{Session::get('flash')}}
                        </div>

                    @endif()
                    <div class="box-body">

                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr class="trHighlight">
                                <th>Sn</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $sn=1;?>
                            @if(count($sliders)>0)
                                @foreach($sliders as $slider)
                                    <tr>
                                        <td>{{$sn}}</td>
                                        <td>{{$slider->title}}</td>
                                        <td><img src="{{asset('public/images/slider/'.$slider->image)}}" alt="" class="img img-responsive" style="width:200px"> </td>
                                        <td>{!!$slider->description!!}</td>
                                        <td>{{$slider->status==1?"Acitve":"Inactive"}}</td>
                                        <td>
                                            <a href="{{url('admin/slider/'.$slider->id.'/edit')}}" ><i class="fa fa-edit" title="Edit" ></i></a>
                                            |
                                            <form action="{{route('admin.slider.destroy',['slider'=>$slider->id])}}" method="POST" style="display: inline">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm(' Are You Sure to Delete?')"><i class="fa fa-trash" title="Delete" ></i></button>
                                            </form>


                                        </td>
                                    </tr>
                                    <?php $sn++;?>
                                @endforeach()

                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->

@endsection
