<!DOCTYPE html>
<html>
<head>

@include('backend.partials.meta')


<!-- Bootstrap 3.3.7 -->
    @include('backend.partials.style')




</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

@include('backend.partials.header')
{{--  <!-- Left side column. contains the logo and sidebar -->--}}


<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    @yield('main-section')

    <!-- Main content -->

        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('backend.partials.footer')


</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
@include('backend.partials.script')

</body>
</html>