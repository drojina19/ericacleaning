<link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('backend/bower_components/font-awesome/css/font-awesome.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{asset('backend/bower_components/Ionicons/css/ionicons.min.css')}}">

<link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<link rel="stylesheet" href="{{asset('backend/bower_components/morris.js/morris.css')}}">

<link rel="stylesheet" href="{{asset('backend/plugins/iCheck/all.css')}}">

<!-- jvectormap --><!-- Date Picker -->
<link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
<!-- Daterange picker --><!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{asset('backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

<link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="stylesheet" href="{{asset('backend/dist/css/AdminLTE.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/dist/css/skins/_all-skins.min.css')}}">