<!-- jQuery 3 -->
<script src="{{asset('backend/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('backend/bower_components/ckeditor/ckeditor.js')}}"></script>
<!-- jQuery UI 1.11.4 -->

<script src="{{asset('backend/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

<script src="{{asset('backend/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('backend/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('backend/bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline --><!-- daterangepicker -->
<script src="{{asset('backend/bower_components/moment/min/moment.min.js')}}"></script>

<script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- datepicker -->
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('backend/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('backend/dist/js/demo.js')}}"></script>
<script src="{{asset('backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
@yield('after-scripts')
<script>
    $(function () {

        //Initialize Select2 Elements
        $('.select2').select2()
        $('.datatable').dataTable()
        $('#datatable').dataTable()


        $('.datepicker').datepicker({
            format:'yyyy-mm-dd',
            autoclose:true
        });

        $("document").ready(function(){
            setTimeout(function(){
                $("div.alert").remove();
            }, 5000 ); // 5 secs

        });


        $.each($("textarea"),function(i,e){
            $(e).addClass("ck");
        });
        $.each($(".ck"),function(i,e){
            if(!$(e).hasClass("nock")){
                // console.log(e);
                CKEDITOR.replace(e);
            }


        });




    });
</script>