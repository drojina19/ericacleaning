@extends('backend.layout.app')
@section('main-section')


    <div class="box-header">
        <div class="box-header with-border">
            <section class="content-header">
                <h1>
                    <b>List Blog</b>
                </h1>

                <div class="col-md-6" style="padding-left: 0px">
                    <div class="col-md-6" style="padding-left: 0px;padding-bottom:5px">
                        <a href="{{route('admin.blog.create')}}" class title="Create">
                                    <span class="badge badge-success">
                                    <i class="fa fa-plus"></i>
                                    </span>
                        </a>
                    </div>
                </div>
            </section>

        </div>
    </div>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <!-- /.box-header -->
                    @if(Session::has('flash'))
                        <div class="alert alert-success">
                            {{Session::get('flash')}}
                        </div>

                    @endif()
                    <div class="box-body">

                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr class="trHighlight">
                                <th>Sn</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Posted_By</th>
                                <th>Posted_Date</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th>Action </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $sn=1;?>
                            @if(count($blogs)>0)
                                @foreach($blogs as $blog)
                                    <tr>
                                        <td>{{$sn}}</td>
                                        <td>{{$blog->title}}</td>
                                        <td>{!!$blog->description!!}</td>
                                        <td><img src="{{asset('public/images/blog/'.$blog->image)}}" alt="" class="img img-responsive" style="width:200px"> </td>
                                        <td>{{$blog->posted_by}}</td>
                                        <td>{{$blog->posted_date}}</td>
                                        <td>{{$blog->type}}</td>
                                        <td>{{$blog->status==1?"Acitve":"Inactive"}}</td>
                                        <td>
                                            <a href="{{url('admin/blog/'.$blog->id.'/edit')}}" ><i class="fa fa-edit" title="Edit" ></i></a>
                                            |
                                            <form action="{{route('admin.blog.destroy',['blog'=>$blog->id])}}" method="POST" style="display: inline">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm(' Are You Sure to Delete?')"><i class="fa fa-trash" title="Delete" ></i></button>
                                            </form>


                                        </td>
                                    </tr>
                                    <?php $sn++;?>
                                @endforeach()

                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->

@endsection
