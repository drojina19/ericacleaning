@extends('backend.layout.app')
@section('main-section')
    <div class="content">
        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <div class="box-header with-border">
                        <section class="content-header">
                            <h1>
                                <b>Edit Services</b>
                            </h1>
                        </section>
                        <a href="{{route('admin.service.create')}}" class="pull-right" title="Create">
                                    <span class="badge badge-success">
                                    <i class="fa fa-plus"></i>
                                    </span>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <form role="form" method="post" action="{{ route('admin.service.update',$service->id) }}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        {{method_field("PATCH")}}


                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="_token" value="{{CSRF_token()}}">
                                    <div class="form-group">
                                        <label for="name" class="control-label">Title *</label>
                                        <input type="text" name="title" id="title" class="form-control" value="{{ $service->title }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="control-label">Slug * </label><small style="color: red">Please enter the same as title except special characters</small>
                                        <input type="text" name="slug" id="title" class="form-control" value="{{ $service->slug }}" required>
                                    </div>



                                    <div class="form-group">
                                        <label for="" class="control-label col-md-2">Image *</label>
                                        <input type="file" name="image" id="image" class="form-control " required>
                                        <small style="color: red">Please upload the image of size 585 x 454 px</small>
                                    </div>

                                    <div class="form-group">
                                        <label for="description" class="control-label">Description</label>
                                        <textarea name="description" id="description" class="form-control" rows="5" required>
                                            {!!  $service->description !!}
                                        </textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="status" class="control-label">Status *</label>
                                        <select name="status" class="form-control" required>
                                            <option value="1" {{ $service->status==1??'selected' }}>Active</option>
                                            <option value="0" {{ $service->status==0??'selected' }}>InActive</option>
                                        </select>
                                    </div>
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-success">update</button>
                                        <a href="{{route('admin.service.index')}}" class="btn btn-danger pull-right">Cancel</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>


                    </form>
                </div>
            </div>
        </div>
    </div>










@endsection