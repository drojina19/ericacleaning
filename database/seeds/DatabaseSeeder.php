<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $this->call(ComponentTableSeeder::class);

        DB::table('users')->insert([

            'name'=>      'EricaCleaning',
            'email'=>     'admin@admin.com',
            'password'=>   bcrypt('12345')
        ]);
    }
}
