<?php

use Illuminate\Database\Seeder;

class ComponentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('components')->insert([

            [
                'title'=>       'about us',
                'description'=> 'description',
                'image'      => 'abc.jpg',
                'status'     => '1'
            ],
            [
                'title'=>       'mission',
                'description'=> 'description',
                'image'      => 'abc.jpg',
                'status'     => '1'
            ],
            [
                'title'=>       'objective',
                'description'=> 'description',
                'image'      => 'abc.jpg',
                'status'     => '1'
            ],
            [
                'title'=>       'people',
                'description'=> 'people',
                'image'      => 'abc.jpg',
                'status'     => '1'
            ]
        ]);
    }
}
