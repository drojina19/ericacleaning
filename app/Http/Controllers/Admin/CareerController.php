<?php

namespace App\Http\Controllers\Admin;

use App\Models\Career;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CareerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $careers=Career::get();
        return view('backend.career.index')
            ->with('careers',$careers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.career.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $career=Career::create($request->all());
        if($request->hasFile('image'))
        {
            $image =  $request->file('image');
            $imageName =  \Illuminate\Support\Str::random('32') . '.' . $image->getClientOriginalExtension();
            //now save the image file
            $image->move('public/images/career', $imageName);
            $career->image = $imageName;
        }
        $career->save();
        Session::flash('flash_success','Added Career Successfully');
        return redirect(route('admin.career.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $career=Career::where('id',$id)->first();
        return view('backend.career.edit')
            ->with('career',$career);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $career=Career::find($id);
        $career->update($request->all());
        if($request->hasFile('image'))
        {
            $image =  $request->file('image');
            $imageName =  \Illuminate\Support\Str::random('32') . '.' . $image->getClientOriginalExtension();
            //now save the image file
            $image->move('public/images/career', $imageName);
            $career->image = $imageName;

        }
        $career->save();
        Session::flash('flash_success','Updated Career Successfully');
        return redirect(route('admin.career.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $career=Career::find($id);
        $career->delete();
        Session::flash('flash_success','Deleted Career Successfully');
        return redirect()->back();
    }
}
