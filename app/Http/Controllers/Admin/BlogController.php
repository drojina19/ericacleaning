<?php

namespace App\Http\Controllers\Admin;

use App\Models\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class BlogController extends Controller
{

    public function index()
    {
        $blogs=Blog::get();
        return view('backend.blog.index')
            ->with('blogs',$blogs);
    }

    public function create()
    {
        return view('backend.blog.create');
    }

    public function store(Request $request)
    {
        $blog=Blog::create($request->all());
        if($request->hasFile('image'))
        {
            $image = $request->file('image');
            $imageName =  \Illuminate\Support\Str::random('32') . '.' . $image->getClientOriginalExtension();
            //now save the image file
            $image->move('public/images/blog', $imageName);
            $blog->image = $imageName;
        }
        $blog->save();
        Session::flash('flash_success','Added Blog Successfully');
        return redirect('admin/blog');
    }

    public function edit($id)
    {
        $blog=Blog::where('id',$id)->first();
        return view('backend.blog.edit')
            ->with('blog',$blog);
    }

    public function update(Request $request, $id)
    {
        $blog=Blog::find($id);
        $blog->update($request->all());
        if($request->hasFile('image'))
        {
            $image = $request->file('image');
            $imageName =  \Illuminate\Support\Str::random('32') . '.' . $image->getClientOriginalExtension();
            //now save the image file
            $image->move('public/images/blog', $imageName);
            $blog->image = $imageName;
        }
        $blog->save();
        Session::flash('flash_success','Updated Blog Successfully');
        return redirect(route('admin.blog.index'));
    }

    public function destroy($id)
    {
      $blog=Blog::find($id);
      $blog->delete();
      Session::flash('flash_success','Deleted Blog Successfully');
      return redirect()->back();
    }
}
