<?php

namespace App\Http\Controllers\Admin;

use App\Models\Component;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class ComponentController extends Controller
{

    public function index()
    {
        $components = Component::get();
        return view('backend.component.index')
            ->with('components', $components);

    }

    public function edit($id)
    {
        $component=Component::where('id',$id)->first();
        return view('backend.component.edit')
            ->with('component',$component);

    }

    public function update(Request $request,$id)
    {
        $component=Component::find($id);
        $component->update($request->all());
        if($request->hasFile('image'))
        {
            $image =  Input::file('image');
            $imageName =  \Illuminate\Support\Str::random('32') . '.' . $image->getClientOriginalExtension();
            //now save the image file
            $image->move('public/images/component', $imageName);
            $component->image = $imageName;
        }

        $component->save();
        Session::flash('flash_success','Updated Component Successfully');
        return redirect(route('admin.component.index'));


    }
}
