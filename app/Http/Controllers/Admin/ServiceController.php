<?php

namespace App\Http\Controllers\Admin;

use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services=Service::get();
        return view('backend.service.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service=Service::create($request->all());
        if($request->hasFile('image'))
        {
            $image =  Input::file('image');
            $imageName =  \Illuminate\Support\Str::random('32') . '.' . $image->getClientOriginalExtension();
            //now save the image file
            $image->move('public/images/service', $imageName);
            $service->image = $imageName;
        }
        $service->save();
        Session::flash('flash_success','Added Successfully');
        return redirect('admin/service');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::where('id', $id)->first();
        return view('backend.service.edit')
            ->with('service', $service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::find($id);
        $service->update($request->all());
        if ($request->hasFile('image')) {
            $image =  Input::file('image');
            $imageName =  \Illuminate\Support\Str::random('32') . '.' . $image->getClientOriginalExtension();
            //now save the image file
            $image->move('public/images/service', $imageName);
            $service->image = $imageName;
        }
        $service->save();
        Session::flash('flash', 'Service Updated Successfully');
        return redirect('admin/service');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::find($id);
        $service->delete();
        Session::flash('flash', 'Deleted Successfully');
        return redirect('admin/service');
    }
}
