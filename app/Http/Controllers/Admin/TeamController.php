<?php

namespace App\Http\Controllers\Admin;

use App\Models\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class TeamController extends Controller
{
    /**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function index()
    {
        $teams=Team::get();
        return view('backend.team.index',compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $team=Team::create($request->all());
        if($request->hasFile('image'))
        {
            $image =  Input::file('image');
            $imageName =  \Illuminate\Support\Str::random('32') . '.' . $image->getClientOriginalExtension();
            //now save the image file
            $image->move('public/images/team', $imageName);
            $team->image = $imageName;
        }
        $team->save();
        Session::flash('flash_success','Added Successfully');
        return redirect('admin/team');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = Team::where('id', $id)->first();
        return view('backend.team.edit')
            ->with('team', $team);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $team = Team::find($id);
        $team->update($request->all());
        if ($request->hasFile('image')) {
            $image =  Input::file('image');
            $imageName =  \Illuminate\Support\Str::random('32') . '.' . $image->getClientOriginalExtension();
            //now save the image file
            $image->move('public/images/team', $imageName);
            $team->image = $imageName;
        }
        $team->save();
        Session::flash('flash', 'Updated Successfully');
        return redirect('admin/team');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = Team::find($id);
        $team->delete();
        Session::flash('flash', 'Deleted Successfully');
        return redirect('admin/team');
    }
}
