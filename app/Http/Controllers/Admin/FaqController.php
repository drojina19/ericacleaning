<?php

namespace App\Http\Controllers\Admin;

use App\Models\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs=Faq::get();
        return view('backend.faq.index')
            ->with('faqs',$faqs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $faq=Faq::create($request->all());
        if($request->hasFile('image'))
        {

            $image =  $request->file('image');
            $imageName =  \Illuminate\Support\Str::random('32') . '.' . $image->getClientOriginalExtension();
            //now save the image file
            $image->move('public/images/faq', $imageName);
            $faq->image = $imageName;
        }
        $faq->save();
        Session::flash('flash_success','Added  Faqs Successfully');
        return redirect(route('admin.faq.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq=Faq::where('id',$id)->first();
        return view('backend.faq.edit')
            ->with('faq',$faq);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $faq=Faq::find($id);
        $faq->update($request->all());
        if($request->hasFile('image'))
        {
            $image =  $request->file('image');
            $imageName =  \Illuminate\Support\Str::random('32') . '.' . $image->getClientOriginalExtension();
            //now save the image file
            $image->move('public/images/faq', $imageName);
            $faq->image = $imageName;

        }

        $faq->save();
        Session::flash('flash_success','Updated Faqs Successfully');
        return redirect(route('admin.faq.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq=Faq::find($id);
        $faq->delete();
        Session::flash('flash_success','Deleted Faqs Successfully');
        return redirect()->back();
    }
}
